<?php
// * * * *
session_start();
include("sessionVariables.php");
if ($_SESSION[$is_logged] != true) {
	 header("location: index.html");
	 exit();
}
// * * * *
function getIfSet(&$value, $default = null)
{
    return isset($value) ? $value : $default;
}

$score = getIfSet($_REQUEST["score"]);

if($score != null){

  include("databaseVariables.php");

  $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basedatos);
    if (!$conexion) {
        die("Fallo: " . mysqli_connect_error());
    }

  //sanitización del nombre
  $nombre = mysqli_real_escape_string($conexion, $_SESSION[$name]);
  $nombre = htmlentities($nombre, ENT_QUOTES);
  $nombre = strip_tags($nombre);

  $sentenciaSQL = "INSERT INTO puntuaciones(name, score, game) VALUES ('" . $nombre . "', " . $score . ", '" . $_REQUEST["game"] . "');";
  $resultado = mysqli_query($conexion, $sentenciaSQL);
	mysqli_close($conexion);
}

session_start();
$_SESSION = array();
session_destroy();

header("location: index.html");
?>
