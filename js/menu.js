function esconder(){
	$("#instrucciones").slideToggle();
}

function inicio() {
	var jsonhttp = new XMLHttpRequest(), url="xml/index.xml";

	jsonhttp.onreadystatechange = function() {
		if (jsonhttp.readyState == 4 && jsonhttp.status == 200) {
			var jsonDoc = jsonhttp.responseText;
			rellenar(jsonDoc);
		}
	};

	jsonhttp.open("GET",url,true);
	jsonhttp.send();

	function rellenar(arr) {

		document.getElementById("prin").innerHTML = arr;

	}
}

function abrir(data, ins){
	var jsonhttp = new XMLHttpRequest(), url="";
	url = data;

	jsonhttp.onreadystatechange = function() {
		if (jsonhttp.readyState == 4 && jsonhttp.status == 200) {
			var jsonDoc = jsonhttp.responseText;
			rellenar(jsonDoc);
		}
	};

	jsonhttp.open("GET",url,true);
	jsonhttp.send();

	function rellenar(arr) {

		document.getElementById("prin").innerHTML = arr;
		setIns(ins);

	}
}

function abrir(data, ins, modal){
	var jsonhttp = new XMLHttpRequest(), url="";
	url = data;

	var modaljsonhttp = new XMLHttpRequest(), modal_url="";
	modal_url = modal;

	jsonhttp.onreadystatechange = function() {
		if (jsonhttp.readyState == 4 && jsonhttp.status == 200) {
			var jsonDoc = jsonhttp.responseText;
			rellenar(jsonDoc);
		}
	};

	modaljsonhttp.onreadystatechange = function() {
		if (modaljsonhttp.readyState == 4 && modaljsonhttp.status == 200) {
			var jsonDoc = modaljsonhttp.responseText;
			rellenarModal(jsonDoc);
		}
	};

	jsonhttp.open("GET",url,true);
	jsonhttp.send();

	modaljsonhttp.open("GET", modal_url, true);
	modaljsonhttp.send();

	function rellenar(arr) {

		document.getElementById("prin").innerHTML = arr;
		setIns(ins);

	}

	function rellenarModal(arr) {

		document.getElementById("game_redirect").innerHTML = arr;

		document.forma.onsubmit = function(){

			var username = document.forma.username.value;
			var validName = (username != "");
			return validName;
		}
	}
}

function setIns(ins) {
	var jsonhttp = new XMLHttpRequest(), url="";
	url = ins;

	jsonhttp.onreadystatechange = function() {
		if (jsonhttp.readyState == 4 && jsonhttp.status == 200) {
			var jsonDoc = jsonhttp.responseText;
			rellenar(jsonDoc);
		}
	};

	jsonhttp.open("GET",url,true);
	jsonhttp.send();

	function rellenar(arr) {

		document.getElementById("instrucciones").innerHTML = arr;

	}
}
