$( init );

var levelOneItems = ['1_resistencia', '2_capacitor', '4_transistor', '6_diodo', '16_bateria', '17_condensador-electrolitico', '18_circuito-integrado', '19_LED', '20_fotocelda', '21_potenciometro', '22_SCR', '23_switch', '24_bocina', '25_protoboard'];
var levelTwoItems = ['1_resistencia', '2_capacitor', '4_transistor', '6_diodo', '16_bateria', '17_condensador-electrolitico', '18_circuito-integrado', '19_LED', '20_fotocelda', '21_potenciometro', '22_SCR', '23_switch', '24_bocina'];
var levelThreeItems = ['1_resistencia', '2_capacitor', '3_inductor', '4_transistor', '6_diodo', '16_bateria', '18_circuito-integrado', '20_fotocelda', '22_SCR', '23_switch', '24_bocina'];
var itemMatrix = [levelOneItems, levelTwoItems, levelThreeItems];
var levelImageTails = ['_AR', '_SIM', '_LE'];

var correctCards = 0;
var totalCorrectCards = 0;

var globalVixen;
var currentLevel = 0;
var maxLevels = itemMatrix.length - 1;
var successfulLevel = false;

var tiempoMaximo = 120;
var iTiempoLimite = tiempoMaximo
var iTiempoTranscurrido = 0;

var currentDescriptionCard = 0;

var descriptionFile = "xml/descMemorama.json";

var gud = new Audio("media/cartoon015.wav");
var bad = new Audio("media/cartoon048.wav");

//===================================
/*Función de inicialización que se encarga de revolver
los elementos del arreglo para el nivel actual, asi como
cargar la descripciones, imágenes y nombres de los componentes*/
//===================================
function init() {

  // Hide the success message
  document.getElementById("texta").style.backgroundColor='white';
  $('#successMessage').hide();
  $('#successMessage').css( {
    left: '580px',
    top: '250px',
    width: 0,
    height: 0
  } );

  // Reset the game
  correctCards = 0;
  $('#cardPile').html( '' );
  $('#cardSlots').html( '' );

  // Create the pile of shuffled cards
  //Si se cambia el nombre al dispositivo aqu[i, hay que cambiarlo tambien en el json
  var numbers = itemMatrix[currentLevel];
  numbers.sort( function() { return Math.random() - .5 } );
  var vixen = numbers.slice(0,5);


  for ( var i=0; i<5; i++ ) {

    $('<div>' + '<img src="media/'+vixen[i].split("_")[1]+levelImageTails[currentLevel]+'.png" width="80" height="80"/>' + '</div>').data( 'number', vixen[i].split("_")[0] ).attr( 'id', 'card'+vixen[i].split("_")[0] ).appendTo( '#cardPile' ).draggable( {
      containment: '#content',
      stack: '#cardPile div',
      cursor: 'move',
      revert: true
    } );
  }

  // Create the card slots
  vixen.sort(function() {return Math.random() - .5});
  //var words = [ 'resistencia', 'inductor', 'capacitor', 'transistor-NPN', 'transistor-PNP', 'diodo', 'opto-acoplador', 'MOSFET', 'trans-formador', 'fuente' ];
  for ( var i=0; i<5; i++ ) {
    $('<div>' + vixen[i].split("_")[1] + '</div>').data( 'number', vixen[i].split("_")[0] ).appendTo( '#cardSlots' ).droppable( {
      accept: '#cardPile div',
      hoverClass: 'hovered',
      drop: handleCardDrop
    } );
  }

  // Create texts
  vixen.sort(function() {return Math.random() - .5});
  var a = 0;
  globalVixen = vixen;

  $.getJSON(descriptionFile, function(json) {
   for ( var i=0; i<json.length; i++ ) {
    if(vixen[a].split("_")[0] == json[i].id){$('#texta').text(json[i].desc).data( 'number', vixen[a].split("_")[0] );}
  }
});

  if(!successfulLevel){
    fntTiempo();
  }
}

//===================================
/*Manejo de movimiento de las cartas, evaluando si los
componentes y su descripción se corresponden*/
//===================================
function handleCardDrop( event, ui ) {
  var slotNumber = $(this).data( 'number' );
  var cardNumber = ui.draggable.data( 'number' );
  var textNumber = $('#texta').data('number');

  // If the card was dropped to the correct slot,
  // change the card colour, position it directly
  // on top of the slot, and prevent it being dragged
  // again

  var isCardDropCorrect = (slotNumber == cardNumber)&&(slotNumber == textNumber)

  if ( isCardDropCorrect ) {

    gud.currentTime = 0;
    gud.play();
    ui.draggable.addClass( 'correct' );
    ui.draggable.draggable( 'disable' );
    $(this).droppable( 'disable' );
    ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
    ui.draggable.draggable( 'option', 'revert', false );
    correctCards++;
    document.getElementById("texta").style.backgroundColor='#66ff66';

    setTimeout(avanza, 1000);

  } else {

    bad.currentTime=0;
    bad.play();
  }

  // If all the cards have been placed correctly then display a message
  // and reset the cards for another go

  if ( correctCards == 5 ) {

    if(currentLevel < maxLevels){

      $('#successMessage').show();
      $('#successMessage').animate( {
        left: '380px',
        top: '200px',
        width: '400px',
        height: '100px',
        opacity: 1
      } );
    } else{

      finishRound();
    }
  }

}

//===================================
/*Manejo de tiempos, refrescamiento del contador en
 pantalla y finalización de ronda por tiempo*/
//===================================
function fntTiempo(){

  if(iTiempoTranscurrido>=iTiempoLimite){
    //finalizar el juego por tiempo
    successfulLevel = false;
    finishRound();
  }else{
    //volvemos a llamar a esta funcion un segundo despues
    setTimeout(fntTiempo, 1000);
    //mostrar el estado del juego
    $('#data').find('b').html('<strong>Tiempo restante: </strong>'+(iTiempoLimite-iTiempoTranscurrido)+' segundos');
    //aumentamos el contador de tiempo transcurido
    iTiempoTranscurrido++;
  }
};

//===================================
/*Manejo del cambio y finalización de las rondas, así
 como la finalización del juego*/
//===================================
function finishRound(){

  if(currentLevel < maxLevels){

    totalCorrectCards = totalCorrectCards + correctCards;
    currentLevel++;
    iTiempoTranscurrido = 0;
    iTiempoLimite = tiempoMaximo;
    init();
  } else{

    totalCorrectCards = totalCorrectCards + correctCards;
    alert("Enhorabuena, has terminado el reto de electrónica con " + totalCorrectCards + " aciertos" );
    window.location.href = "saveScore.php?game=retoelectronica1&score=" + totalCorrectCards;
  }
}

function nextLevel(){
  successfulLevel = true;
  finishRound();
}

//============================================
/*Manejo del avance y regreso de las cartas de
descripción de los componentes*/
//============================================
function regresa(){

  var vixen = globalVixen;
  $.getJSON(descriptionFile, function(json) {

    if(currentDescriptionCard == 0) {
      currentDescriptionCard = 4;
    }else {
      currentDescriptionCard = currentDescriptionCard - 1;
    }

    for ( var i=0; i<json.length; i++ ) {
      if(vixen[currentDescriptionCard].split("_")[1] == json[i].names) $('#texta').text(json[i].desc).data( 'number', vixen[currentDescriptionCard].split("_")[0] );
    }

    if($('#card'+$('#texta').data('number')).hasClass('correct')){
      document.getElementById("texta").style.backgroundColor='#66ff66';
    }else{
      document.getElementById("texta").style.backgroundColor='white';
    }
  });
}

function avanza(){

  var vixen = globalVixen;
  $.getJSON(descriptionFile, function(json) {

    if(currentDescriptionCard == 4) {
      currentDescriptionCard = 0;
    }else {
      currentDescriptionCard = currentDescriptionCard + 1;
    }

    for ( var i=0; i<json.length; i++ ) {
      if(vixen[currentDescriptionCard].split("_")[1] == json[i].names) $('#texta').text(json[i].desc).data( 'number', vixen[currentDescriptionCard].split("_")[0] );
    }

    if($('#card'+$('#texta').data('number')).hasClass('correct')){
      document.getElementById("texta").style.backgroundColor='#66ff66';
    }else {
      document.getElementById("texta").style.backgroundColor='white';
    }
  });
}
