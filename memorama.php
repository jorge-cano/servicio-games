<?php
// * * * *
session_start();
include("sessionVariables.php");
if ($_SESSION[$is_logged] != true) {
	 header("location: index.html");
	 exit();
}
// * * * *
?>
<!DOCTYPE html>
<html lang="en">
<head>

<title>Reto Electrónica 2</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css">

<script type="text/javascript" src="js/jquery-1.5.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/memorama.js"></script>

</head>
<body>

<header>
  <audio autoplay="autoplay" loop="loop" width="300" height="32" ><source src="media/scifi.mp3" type="audio/mp3"></source></audio>
	<center><h1>Reto Electrónica 2</h1>
		<div id="data">
  			<b></b>
		</div>
	</center>
</header>

<div id="content">

  <div id="cardPile"> </div>
  <div id="cardSlots"> </div>

  <div id="desc">
    <a href="#" class="izquierda_flecha" id="izquierda" onclick="regresa();"><img src="media/flecha_01.png"  /></a>
    <div class="wideBox" id="texta"></div>
    <a href="#" class="derecha_flecha" id="derecha" onclick="avanza();"><img src="media/flecha_02.png"  /></a>
  </div>



  <div id="successMessage">
    <h2>Eureka!</h2>
    <button onclick="nextLevel();">Next Level</button>
  </div>

</div>


</body>
</html>
